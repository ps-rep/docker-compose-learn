FROM ubuntu:20.04

EXPOSE 3306 80

ENV db_name=laravel db_user=laravel db_password=secret

#install php and some extensions
RUN apt update -qqy && \
    apt upgrade -qqy && \
    apt install -y software-properties-common && \
    add-apt-repository -y ppa:ondrej/php && \
    apt update -qqy && \
    apt install -y php7.4 php7.4-bcmath php7.4-bz2 php7.4-cgi \
                     php7.4-curl php7.4-dba php7.4-fpm php7.4-gd \
                     php7.4-intl php7.4-json php7.4-mbstring php7.4-mysql \
                     php7.4-opcache php7.4-soap php7.4-readline php7.4-xml \
                     php7.4-xsl php7.4-zip && \
    apt install -y nginx mysql-server-8.0

RUN sed -Ei 's/^(bind-address|log)/#&/' /etc/mysql/mysql.conf.d/mysqld.cnf

RUN rm -rf /var/www/html/*
# COPY ./src /var/www/html/

RUN chown -R www-data:www-data /var/www/

COPY ./build/ubuntu/nginx.conf /etc/nginx/sites-available/default

CMD service mysql start && mysql -e "CREATE DATABASE IF NOT EXISTS ${db_name} DEFAULT \
    CHARACTER SET utf8 COLLATE utf8_unicode_ci; GRANT ALTER,DELETE,DROP,CREATE,INDEX,INSERT, \
    SELECT,UPDATE,CREATE TEMPORARY TABLES,LOCK TABLES ON ${db_name}.* TO '${db_user}'@'%' \
    IDENTIFIED BY '${db_password}';FLUSH PRIVILEGES;"

WORKDIR /var/www/html/

CMD service php7.4-fpm start && service mysql start && nginx -g "daemon off;"